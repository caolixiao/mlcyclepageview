

Pod::Spec.new do |s|

  s.name         = "MLCyclePageView"
  s.version      = "1.0.0"
  s.summary      = "A short description of MLCyclePageView."

  s.description  = <<-DESC
                   A longer description of MLCyclePageView in Markdown format.

                   * Think: Why did you write this? What is the focus? What does it do?
                   * CocoaPods will be using this to generate tags, and improve search results.
                   * Try to keep it short, snappy and to the point.
                   * Finally, don't worry about the indent, CocoaPods strips it!
                   DESC

  s.homepage     = "https://caolixiao@bitbucket.org/caolixiao/mlcyclepageview"
  s.license      = "MIT"
  s.author             = { "caolixiao" => "caolixiao@yeah.net" }
  s.platform     = :ios, "7.0"
  s.source       = { :git => "https://caolixiao@bitbucket.org/caolixiao/mlcyclepageview.git", :tag => "#{s.version}" }
  s.source_files  = "MLCyclePageView", "MLCyclePageView/*.{h,m}"
  s.public_header_files = "MLCyclePageView/*.h"
	s.resource  = "MLCyclePageView/Images.xcassets"
  s.requires_arc = true

end
