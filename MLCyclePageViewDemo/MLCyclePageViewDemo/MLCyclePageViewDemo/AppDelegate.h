//
//  AppDelegate.h
//  MLCyclePageViewDemo
//
//  Created by caolixiao on 2017/2/23.
//  Copyright © 2017年 toby. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

