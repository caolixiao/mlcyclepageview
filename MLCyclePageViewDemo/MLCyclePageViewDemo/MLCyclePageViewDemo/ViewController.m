//
//  ViewController.m
//  MLCyclePageViewDemo
//
//  Created by caolixiao on 2017/2/23.
//  Copyright © 2017年 toby. All rights reserved.
//

#import "ViewController.h"
#import <MLCyclePageView.h>

@interface ViewController () <MLCyclePageViewDataSource>
{
    MLCyclePageView * cycleView;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor whiteColor];
    
    cycleView = [[MLCyclePageView alloc] init];
    cycleView.backgroundColor = [UIColor whiteColor];
    cycleView.dataSource = self;
    [self.view addSubview:cycleView];
    cycleView.frame = CGRectMake(0.0f, 60.0f, self.view.bounds.size.width, self.view.bounds.size.height - 60.0f);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - MLCyclePageViewDataSource
- (NSUInteger) numberOfItemsInCyclePageView:(MLCyclePageView *) cyclePageView
{
    return 7;
}

- (UIView *) cyclePageView:(MLCyclePageView *) cyclePageView viewForItemAtIndex:(NSUInteger) index resetView:(UIView *) vie
{
    UIView *view = [[UIView alloc] init];
    view.frame = cycleView.bounds;
    UILabel *lab = [[UILabel alloc] initWithFrame:view.bounds];
    lab.textAlignment = NSTextAlignmentCenter;
    lab.font = [UIFont systemFontOfSize:40];
    lab.text = [NSString stringWithFormat:@"%d", (int)index];
    if (index%2 == 0) lab.backgroundColor = [UIColor whiteColor];
    else lab.backgroundColor = [UIColor whiteColor];
    
    [view addSubview:lab];
    return view;
}

//
- (MLCyclePageHeadViewType) cyclePageViewTypeInTarget:(MLCyclePageView *) cyclePageView
{
    return MLCyclePageHeadViewTypePage;
}

- (BOOL) cyclePageView:(MLCyclePageView *) cyclePageView shadowAtIndex:(NSUInteger) index
{
    return YES;
}

@end
