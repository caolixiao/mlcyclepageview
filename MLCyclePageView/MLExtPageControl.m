//
//  MLExtPageControl.m
//

#import "MLExtPageControl.h"

@implementation MLExtPageControl

@synthesize type;
@synthesize wPoint;

- (instancetype) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {

    }
    return self;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    if (wPoint == 0) return;
    
    CGFloat x = (self.bounds.size.width - (self.numberOfPages * wPoint*2))/2.0f;
    int i = 0;
    for (UIView *v in self.subviews) {
        if (![v isKindOfClass:[UIImageView class]]) {
            v.frame = CGRectMake(x + (wPoint*2 * i), (self.bounds.size.height - wPoint)/2.0f, wPoint, wPoint);
            v.layer.cornerRadius = wPoint/2.0f;
            i++;
        }
    }
    
    if (MLExtPageControlTypeArrow == type) {
        if (!ivLeft) [self buildView];
        
        ivLeft.frame = CGRectMake(x - 15.0f, (self.bounds.size.height - 10.0f)/2.0f, 7.0f, 10.0f);
        ivRight.frame = CGRectMake(x + (self.numberOfPages * (wPoint*2)) + 5.0f, (self.bounds.size.height - 10.0f)/2.0f, 7.0f, 10.0f);
    }
    else
    {
        if (ivLeft) [ivLeft removeFromSuperview];
        if (ivRight) [ivRight removeFromSuperview];
    }
}

- (void) buildView
{
    ivLeft = [[UIImageView alloc] init];
    ivLeft.image = [UIImage imageNamed:@"report-l-arrow-"];
    [self addSubview:ivLeft];
    
    ivRight = [[UIImageView alloc] init];
    ivRight.image = [UIImage imageNamed:@"report-r-arrow-"];
    [self addSubview:ivRight];
}


@end
