//
//  MLExtPageControl.h
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, MLExtPageControlType) {
    MLExtPageControlTypeDefault = 0,
    MLExtPageControlTypeArrow = 1,
};


@interface MLExtPageControl : UIPageControl
{
    @private
    UIImageView *ivLeft;
    UIImageView *ivRight;
}

@property (nonatomic, assign) MLExtPageControlType type;

// 设置点宽
@property (nonatomic, assign) float wPoint;

@end
