//
//  MLCyclePageView.m
//

#import "MLCyclePageView.h"

#import <QuartzCore/QuartzCore.h>
#import <objc/runtime.h>
#import "MLExtPageControl.h"

#define SCROLL_DURATION 0.25f

#define PerformSelectorPreTrans(argc) \
do { \
    [CATransaction begin];\
    [CATransaction setDisableActions:YES];\
    CATransform3D transform = CATransform3DIdentity;\
    transform.m34 = perspective;\
    CGFloat y = -fabs(((self.bounds.size.width - fabs(argc))*15)/self.bounds.size.width);\
    CGFloat z = -fabs(((self.bounds.size.width - fabs(argc))*10)/self.bounds.size.width);\
    transform = CATransform3DTranslate(transform, 0.0f, y, z);\
    preView.layer.transform = transform;\
    [CATransaction setDisableActions:NO];\
    [CATransaction commit];\
} while (0)


#define PerformSelectorCurTrans(argc) \
do { \
    [CATransaction begin];\
    [CATransaction setDisableActions:YES];\
    CATransform3D transform = CATransform3DIdentity;\
    transform.m34 = perspective;\
    transform = CATransform3DTranslate(transform, argc, 0.0f, 0.0f);\
    currentView.layer.transform = transform;\
    [CATransaction setDisableActions:NO];\
    [CATransaction commit];\
} while (0)


#define PerformSelectorNextTrans(argc) \
do { \
    [CATransaction begin];\
    [CATransaction setDisableActions:YES];\
    CATransform3D transform = CATransform3DIdentity;\
    transform.m34 = perspective;\
    CGFloat y = -fabs(((self.bounds.size.width - fabs(argc))*15)/self.bounds.size.width);\
    CGFloat z = -fabs(((self.bounds.size.width - fabs(argc))*10)/self.bounds.size.width);\
    transform = CATransform3DTranslate(transform, 0.0f, y, z);\
    nextView.layer.transform = transform;\
    [CATransaction setDisableActions:NO];\
    [CATransaction commit];\
} while (0)

#define PerformSelectorDefualtTransWithView(__view, index) \
do { \
    [CATransaction begin];\
    [CATransaction setDisableActions:YES];\
    CATransform3D transform = CATransform3DIdentity;\
    transform.m34 = perspective;\
    if (currentNumberOfItem == index)\
    {\
        transform = CATransform3DTranslate(transform, 0.0f, 0.0f, 0.0f);\
        __view.layer.transform = transform;\
    }\
    else if ([self preAtIndex] == index)\
    {\
        transform = CATransform3DTranslate(transform, 0.0f, -15.0f, -10.0f);\
        __view.layer.transform = transform;\
    }\
    else if ([self nextAtIndex] == index)\
    {\
        transform = CATransform3DTranslate(transform, 0.0f, -15.0f, -10.0f);\
        __view.layer.transform = transform;\
    }\
    [CATransaction setDisableActions:NO];\
    [CATransaction commit];\
} while (0)

#define PerformSelectorDefualtTrans(index)\
do { \
    if (currentNumberOfItem == index) PerformSelectorDefualtTransWithView(currentView, index);\
    else if ([self preAtIndex] == index) PerformSelectorDefualtTransWithView(preView, index);\
    else if ([self nextAtIndex] == index) PerformSelectorDefualtTransWithView(nextView, index);\
} while (0)



@implementation MLCyclePageView

@synthesize dataSource = _dataSource;

@synthesize contentView;

@synthesize numberOfItems;
@synthesize currentNumberOfItem;
@synthesize m_Items;

@synthesize headType;

@synthesize titleColor;
@synthesize titleFont;
@synthesize scrollEnabled;

@synthesize currentView;

- (void) dealloc
{
    objc_removeAssociatedObjects(self);
}

- (instancetype) init
{
    if (self = [super init]) {
        [self initDataAndView];
    }
    return self;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    contentView.frame = self.bounds;
    [self reloadData];
}

- (void) initDataAndView
{
    // init data
    numberOfItems = 0;
    currentNumberOfItem = 0;
    headType = MLCyclePageHeadViewTypeDefault;
    pageStatus = MLCyclePageViewPageStatusNone;
    perspective = -1.0f/500.0f;
    head_height = 0; //26.0f;
    
    scrollEnabled = YES;
    scrollOffset = 0.0f;
    startPointX = 0.0f;

    // init view
    contentView = [[UIView alloc] initWithFrame:self.bounds];
    [self addSubview:contentView];
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(actionGlobalPanWithGesture:)];
    [contentView addGestureRecognizer:panGesture];
}

#pragma mark - set show data info 
- (void) loadHeadShowData
{
    if (labHeadInfo) labHeadInfo.text = [NSString stringWithFormat:@"%d/%d", (int)(currentNumberOfItem + 1), (int)numberOfItems];
    else if (pcHeadInfo) pcHeadInfo.currentPage = currentNumberOfItem;
}

#pragma mark - data
- (void) reloadData
{
    [contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    if (!_dataSource) return;
    
    numberOfItems = [_dataSource numberOfItemsInCyclePageView:self];
    m_Items = [NSMutableDictionary dictionaryWithCapacity:numberOfItems];
    
    [self loadHeadView];
    [self loadDefualtView];
    
    [self loadBufferItemViewAtIndex:[self nextAtIndex] bufView:nextView];
    [self loadBufferItemViewAtIndex:currentNumberOfItem bufView:currentView];
    [self loadBufferItemViewAtIndex:[self preAtIndex] bufView:preView];
}

- (void) setShowView:(UIView *)view forIndex:(NSInteger)index
{
    [m_Items setObject:view forKey:[NSNumber numberWithInteger:index]];
}

- (UIView *) itemViewAtIndex:(NSInteger) index
{
    return [m_Items objectForKey:[NSNumber numberWithInteger:index]];
}

- (NSUInteger) preAtIndex
{
    return (currentNumberOfItem <= 0) ? (numberOfItems - 1) : (currentNumberOfItem - 1);
}

- (NSUInteger) nextAtIndex
{
    return (currentNumberOfItem >= numberOfItems-1) ? 0 : (currentNumberOfItem + 1);
}

- (MLCyclePageViewPageStatus) getPageStatusWithDirection:(NSInteger) direction
{
    if (direction == 1) return MLCyclePageViewPageStatusNext;
    else if (direction == -1) return MLCyclePageViewPageStatusPre;
    else return MLCyclePageViewPageStatusNone;
}

#pragma mark - set layout view
- (void) loadHeadView
{
    if ([_dataSource respondsToSelector:@selector(cyclePageViewTypeInTarget:)])
        headType = [_dataSource cyclePageViewTypeInTarget:self];
    
    if (vHead) [vHead.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    switch (headType) {
        case MLCyclePageHeadViewTypeNone:
            if (vHead) {
                head_height = 0.0f;
                [vHead removeFromSuperview];
                vHead = nil;
            }
            break;
        case MLCyclePageHeadViewTypeNumber | MLCyclePageHeadViewTypeDefault:
        {
            head_height = 26.0f;
            if (!vHead)
            {
                vHead = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.bounds.size.width, head_height)];
            }
            if (!labHeadInfo)
            {
                labHeadInfo = [[UILabel alloc] initWithFrame:vHead.bounds];
                labHeadInfo.textAlignment = NSTextAlignmentCenter;
            }
            if(titleFont) labHeadInfo.font = titleFont; else labHeadInfo.font = [UIFont systemFontOfSize:14.0f];
            if (titleColor) labHeadInfo.textColor = titleColor; else labHeadInfo.textColor = [UIColor blackColor];
            [self loadHeadShowData];
            [vHead addSubview:labHeadInfo];
            
            if (![contentView.subviews containsObject:vHead]) [contentView addSubview:vHead];
        }
            break;
        case MLCyclePageHeadViewTypePage:
        {
            head_height = 26.0f;
            if (!vHead)
            {
                vHead = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 6.0f, self.bounds.size.width, head_height)];
            }

            if (!pcHeadInfo)
            {
                pcHeadInfo = [[MLExtPageControl alloc] initWithFrame:vHead.bounds];
                pcHeadInfo.type = MLExtPageControlTypeArrow;
                pcHeadInfo.wPoint = 4.0f;
                pcHeadInfo.pageIndicatorTintColor = [UIColor colorWithRed:149.0/255.0f green:149.0f/255.0f blue:149.0f/255.0f alpha:1];
                pcHeadInfo.currentPageIndicatorTintColor =  [UIColor colorWithRed:22.0f/255.0f green:99.0f/255.0f blue:126.0f/255.0f alpha:1];

                [pcHeadInfo addTarget:self action:@selector(actionPcHeadinfo:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            pcHeadInfo.numberOfPages = numberOfItems;
            [self loadHeadShowData];
            [vHead addSubview:pcHeadInfo];
            
            if (![contentView.subviews containsObject:vHead]) [contentView addSubview:vHead];
        }
            break;
        default:
            break;
    }
}


- (void) loadDefualtView
{
    CGRect frame = CGRectMake(11.0f, head_height + 20.0f, self.bounds.size.width - 22.0f, self.bounds.size.height - head_height - 16.0f - 15.0f);
    
    if (!currentView) {
        preView = [[UIView alloc] initWithFrame:frame];
        currentView = [[UIView alloc] initWithFrame:frame];
        nextView = [[UIView alloc] initWithFrame:frame];
    }
    
    if (![currentView.subviews containsObject:preView]) [contentView addSubview:preView];
    if (![currentView.subviews containsObject:nextView]) [contentView addSubview:nextView];
    if (![currentView.subviews containsObject:currentView]) [contentView addSubview:currentView];
    
    preView.backgroundColor = currentView.backgroundColor = nextView.backgroundColor = [UIColor whiteColor];
}


- (UIView *) loadBufferItemViewAtIndex:(NSInteger) index bufView:(UIView *) bufView
{
    [bufView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    UIView *view = nil;
    BOOL isShadow = NO;
    if ([_dataSource respondsToSelector:@selector(cyclePageView:viewForItemAtIndex:resetView:)])
        view = [_dataSource cyclePageView:self viewForItemAtIndex:index resetView:[self itemViewAtIndex:index]];
    
    if ([_dataSource respondsToSelector:@selector(cyclePageView:shadowAtIndex:)])
        isShadow = [_dataSource cyclePageView:self shadowAtIndex:index];
    
    [bufView addSubview:view];
    [bufView setRadius:5.0f];
    [bufView setShadow:isShadow];
    PerformSelectorDefualtTransWithView(bufView, index);
    [self setShowView:view forIndex:index];
    
    [contentView bringSubviewToFront:currentView];
    
    return view;
}

- (void) resetDefualtView
{
    if (pageStatus == MLCyclePageViewPageStatusPre) {
        currentNumberOfItem = (currentNumberOfItem <= 0) ? numberOfItems - 1 : (currentNumberOfItem - 1);
        
        UIView *vTmp = nextView;
        nextView = currentView;
        currentView = preView;
        preView = vTmp;
        
        [self loadBufferItemViewAtIndex:[self preAtIndex] bufView:preView];
        PerformSelectorDefualtTrans([self nextAtIndex]);
    }
    else if (pageStatus == MLCyclePageViewPageStatusNext)
    {
        currentNumberOfItem = (currentNumberOfItem >= numberOfItems-1) ? 0 : (currentNumberOfItem + 1);
        
        UIView *vTmp = preView;
        preView = currentView;
        currentView = nextView;
        nextView = vTmp;
        
        [self loadBufferItemViewAtIndex:[self nextAtIndex] bufView:nextView];
        PerformSelectorDefualtTrans([self preAtIndex]);
    }
    
    if (headType != MLCyclePageHeadViewTypeNone) [self loadHeadShowData];
}

#pragma mark - head action
- (void) actionPcHeadinfo:(UIPageControl *)  page
{
    if (page.currentPage < currentNumberOfItem)
        pageStatus = MLCyclePageViewPageStatusPre;
    else if(page.currentPage > currentNumberOfItem)
        pageStatus = MLCyclePageViewPageStatusNext;
    else return;
    
    startPointX = 0.0f;
    scrollDuration = SCROLL_DURATION;
    startTime = CACurrentMediaTime();
    
    [self startAnimation];
}


#pragma mark - UIPanGestureRecognizer methon
- (void) actionGlobalPanWithGesture:(UIPanGestureRecognizer *) panGesture
{
    if (!scrollEnabled) return;

    switch (panGesture.state)
    {
        case UIGestureRecognizerStateBegan:
        {
            beganPointX = [panGesture translationInView:self].x;
            break;
        }
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
        {
            if (fabs(startPointX) < 30)
            {
                decelerating = YES;
                pageStatus = [self getPageStatusWithDirection:-((int)(startPointX / fabs(startPointX)))];
                startTime = CACurrentMediaTime();
                scrollDuration = SCROLL_DURATION/3.0f;
                [self startAnimation];
            }
            else
            {
                decelerating = NO;
                pageStatus = [self getPageStatusWithDirection:(int)(startPointX / fabs(startPointX))];
                startTime = CACurrentMediaTime();
                scrollDuration = SCROLL_DURATION;
                [self startAnimation];
            }
        }
            break;
        default:
        {
            startPointX = [panGesture translationInView:self].x - beganPointX;

            pageStatus = [self getPageStatusWithDirection:(int)(startPointX / fabs(startPointX))];
            if (MLCyclePageViewPageStatusNext == pageStatus) PerformSelectorNextTrans(startPointX);
            else PerformSelectorPreTrans(startPointX);
            PerformSelectorCurTrans(startPointX);
        }
            break;
    }
}

#pragma mark -  CATransform3D
- (void)cycleAnimation
{
    NSTimeInterval currentTime = CACurrentMediaTime();

    BOOL isAnimation = NO;
    
    NSTimeInterval time = fminf(1.0f, (currentTime - startTime) / scrollDuration);
    CGFloat delta = [self easeInOut:time];
    
    if (decelerating)
        scrollOffset = -(fabs(startPointX) - fabs(startPointX) * delta);
    else
        scrollOffset = (fabs(startPointX) + (fabs(self.bounds.size.width) - fabs(startPointX)) * delta);
    
    if (pageStatus == MLCyclePageViewPageStatusPre)
    {
        scrollOffset = -scrollOffset;
        PerformSelectorPreTrans(scrollOffset);
    }
    else if (pageStatus == MLCyclePageViewPageStatusNext)
    {
        PerformSelectorNextTrans(scrollOffset);
    }
    
    PerformSelectorCurTrans(scrollOffset);
    
    if (time == 1.0f) isAnimation = YES;
    
    if (isAnimation) {
         if (!decelerating) [self resetDefualtView];
        scrollOffset = 0;
        pageStatus = MLCyclePageViewPageStatusNone;
        [self stopAnimation];
    }
}

- (void)startAnimation
{
    if (!timer) {
        timer = [NSTimer timerWithTimeInterval:1.0/60.0 target:self selector:@selector(cycleAnimation) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
    }
}

- (void)stopAnimation
{
    if (timer) {
        [timer invalidate];
        timer = nil;
    }
}

- (CGFloat) easeInOut:(CGFloat)time
{
    return (time < 0.5f)? 0.5f * powf(time * 2.0f, 3.0f): 0.5f * powf(time * 2.0f - 2.0f, 3.0f) + 1.0f;
}


@end


#pragma mark - 
#pragma mark -
@implementation UIView (shadow)

static const char *__LX__radius__ = "__LX__radius__";
static const char *__LX__isRadius__ = "__LX__isRadius__";

@dynamic radius, isRadiusOfObj;

- (void) setIsRadiusOfObj:(NSArray *) isRadiusOfObj
{
    if (isRadiusOfObj && isRadiusOfObj.count > 2) {
        objc_setAssociatedObject(self, __LX__isRadius__, isRadiusOfObj, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        self.clipsToBounds = [[isRadiusOfObj objectAtIndex:0] boolValue];
        self.layer.cornerRadius = [[isRadiusOfObj objectAtIndex:1] floatValue];
        self.layer.masksToBounds = YES;
        self.frame = CGRectFromString([isRadiusOfObj objectAtIndex:2]);
    }
}

- (NSNumber *) isRadiusOfObj
{
    return objc_getAssociatedObject(self, __LX__isRadius__);
}

- (void) setRadius:(CGFloat) radius
{
    objc_setAssociatedObject(self, __LX__radius__, [NSNumber numberWithFloat:radius], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    self.layer.cornerRadius = radius;
    self.layer.masksToBounds = YES;
}


- (CGFloat) radius
{
    return [objc_getAssociatedObject(self, __LX__radius__) floatValue];
}

- (void) setShadow:(BOOL) shadow
{
    if (self.radius > 0)
        [self.subviews makeObjectsPerformSelector:@selector(setIsRadiusOfObj:)
                                       withObject:@[[NSNumber numberWithBool:YES],
                                                    [NSNumber numberWithFloat:self.radius],
                                                    NSStringFromCGRect(self.bounds)]];

    if (shadow) {
        self.layer.shadowOffset = CGSizeMake(0, 0);
        self.layer.shadowRadius = 3.0f;
        self.layer.shadowOpacity = 0.8f;
        self.layer.shadowColor = [UIColor lightGrayColor].CGColor;
        self.layer.masksToBounds = NO;
        
        UIBezierPath *path = [UIBezierPath bezierPath];
        
        float width = self.bounds.size.width;
        float height = self.bounds.size.height;
        float x = self.bounds.origin.x;
        float y = self.bounds.origin.y;
        float addWH = 3;
        
        [path moveToPoint:self.bounds.origin];
        [path addQuadCurveToPoint:CGPointMake(x+width,y) controlPoint:CGPointMake(x+(width/2),y-addWH)];
        [path addQuadCurveToPoint:CGPointMake(x+width,y+height) controlPoint:CGPointMake(x+width+addWH,y+(height/2))];
        [path addQuadCurveToPoint:CGPointMake(x,y+height) controlPoint:CGPointMake(x+(width/2),y+height+addWH)];
        [path addQuadCurveToPoint:self.bounds.origin controlPoint:CGPointMake(x-addWH,y+(height/2))];
        self.layer.shadowPath = path.CGPath;
    }
    else
    {
        self.layer.shadowOffset = CGSizeMake(0, -3);
        self.layer.shadowRadius = 3.0f;
        self.layer.shadowOpacity = 0.0f;
        self.layer.shadowColor = [UIColor clearColor].CGColor;
        self.layer.shadowPath = nil;
        self.layer.masksToBounds = YES;
    }
}

@end
